# Paste

A minimal pastebin written in Python using Flask.

## Configuration
Keys can be generated with `secrets.token_hex(32)`.
- `SECRET_KEY`
- `ACCESS_KEY`
