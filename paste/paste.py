from flask import Blueprint, render_template, request, flash, redirect, url_for

from paste.db import get_db
from paste.auth import auth_required

bp = Blueprint("paste", __name__)


@bp.route("/list")
@auth_required
def list():
    db = get_db()
    pastes = db.execute("SELECT * FROM paste ORDER BY id").fetchall()

    return render_template("paste/list.html", pastes=pastes)


@bp.route("/", methods=["GET", "POST"])
@auth_required
def paste():
    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required"
        if len(title) > 128:
            error = "Title may not exceed 128 characters"
        if len(body.encode("utf-8")) > 5242880:
            error = "Paste body may not exceed 5MB"

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute("INSERT INTO paste (title, body) VALUES (?, ?)",
                       (title, body))
            db.commit()

            id = db.execute("SELECT MAX(id) FROM paste").fetchone()[0]
            return redirect(url_for("paste.show", id=id))

    return render_template("paste/paste.html")


@bp.route("/<int:id>")
@auth_required
def show(id):
    db = get_db()
    paste = dict(
        db.execute("SELECT title, body FROM paste WHERE id = ?",
                   (id, )).fetchone())

    return render_template("paste/show.html", paste=paste)
