import os

from flask import Flask

from . import db
from . import paste
from . import auth


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "paste.sqlite"),
    )

    if test_config:
        app.config.from_mapping(test_config)
    else:
        app.config.from_pyfile("config.py", silent=True)

    os.makedirs(app.instance_path, exist_ok=True)
    db.init_app(app)

    app.register_blueprint(paste.bp)
    app.register_blueprint(auth.bp)

    return app
