import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app

bp = Blueprint("auth", __name__)


@bp.route("/auth", methods=["GET", "POST"])
def auth():
    if request.method == "POST":
        key = request.form["key"]

        if key == current_app.config["ACCESS_KEY"]:
            session.clear()
            session["auth"] = True
            return redirect(url_for("paste.paste"))

        flash("Invalid key.")

    return render_template("auth/auth.html")


@bp.route("/deauth")
def deauth():
    session.clear()
    return redirect(url_for("auth.auth"))


@bp.before_app_request
def check_auth():
    auth = session.get("auth")

    if auth is None:
        g.auth = None
    else:
        g.auth = True


def auth_required(view):

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.auth is None:
            return redirect(url_for("auth.auth"))

        return view(**kwargs)

    return wrapped_view
